package main

import (
	"fmt"
	"html/template"
	"net/http"
	"log"
	"golang.org/x/crypto/acme/autocert"
)

var t *template.Template

func homePage(w http.ResponseWriter, r *http.Request) {
	err := t.ExecuteTemplate(w, "homePage", nil)
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}
}

func aboutPage(w http.ResponseWriter, r *http.Request) {
	err := t.ExecuteTemplate(w, "aboutPage", nil)
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}
}

func contactPage(w http.ResponseWriter, r *http.Request) {
	err := t.ExecuteTemplate(w, "contactPage", nil)
	if err != nil {
		fmt.Println(http.StatusInternalServerError)
	}
}

func redirect(w http.ResponseWriter, req *http.Request) {
	target := "https://" + req.Host + req.URL.Path
	if len(req.URL.RawQuery) > 0 {
		target += "?" +req.URL.RawQuery
	}
	http.Redirect(w, req, target, http.StatusTemporaryRedirect)
}

func main() {

	var err error

	//port := os.Getenv("PORT")
	//if port == "" {
	//	port = "3000"
	//}

	mux := http.NewServeMux()
	fs := http.FileServer(http.Dir("assets"))

	t, err = template.ParseFiles(
		"templates/aboutPage.html",
		"templates/contactPage.html",
		"templates/footerComponent.html",
		"templates/headComponent.html",
		"templates/homePage.html",
		"templates/navComponent.html",
	)
	if err != nil {
		panic(err)
	}

	mux.Handle("/assets/", http.StripPrefix("/assets/", fs))

	mux.HandleFunc("/", homePage)
	mux.HandleFunc("/about", aboutPage)
	mux.HandleFunc("/contact", contactPage)

	go http.ListenAndServe(":80", http.HandlerFunc(redirect))
	fmt.Println("The server is running on port: 443")
	log.Fatal(http.Serve(autocert.NewListener("rektaf.io"), mux))
}
